﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Boxcar
{
	[Serializable]
	public class MetaData : ISerializable
	{
		public string Name { get; set; } = string.Empty;
		public DateTime Created { get; set; } = DateTime.Now;
		public DateTime Modified { get; set; } = DateTime.Now;
		public DateTime Accessed { get; set; } = DateTime.Now;
		public List<string> Tags { get; set; } = new List<string>();
		public Dictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();

		public MetaData()
		{
		}

		protected MetaData(SerializationInfo info, StreamingContext context)
		{
			Name = info.GetString("name");
			Created = DateTime.Parse(info.GetString("created"));
			Modified = DateTime.Parse(info.GetString("modified"));
			Accessed = DateTime.Parse(info.GetString("accessed"));
			var tags = info.GetValue("tags", typeof(IEnumerable)) as IEnumerable;
			Tags = new List<string>();
			foreach (var tag in tags) { Tags.Add(tag.ToString()); }
			Properties = info.GetValue("properties", typeof(Dictionary<string, object>)) as Dictionary<string, object>;
		}

		/// <summary>
		/// Get the SerializationInfo
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("name", Name);
			info.AddValue("created", Created.ToString("o"));
			info.AddValue("modified", Modified.ToString("o"));
			info.AddValue("accessed", Accessed.ToString("o"));
			info.AddValue("tags", Tags);
			info.AddValue("properties", Properties);
		}

		public IDictionary<string, Object> ToDictionary()
		{
			var info = new SerializationInfo(typeof(Document), new FormatterConverter());
			GetObjectData(info, new StreamingContext());
			var data = new Dictionary<string, object>();
			SerializationInfoEnumerator e = info.GetEnumerator();
			while (e.MoveNext())
			{
				data.Add(e.Name, e.Value);
			}
			return data;
		}
	}
}