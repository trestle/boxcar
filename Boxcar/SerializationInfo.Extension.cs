﻿using System.Runtime.Serialization;

namespace Boxcar
{
	public static class SerializationInfoExtension
	{
		public static int ComputeHashCode(this SerializationInfo info)
		{
			unchecked
			{
				int hash = 27;
				hash = (13 * hash) + info.MemberCount.GetHashCode();
				var enumerator = info.GetEnumerator();
				while (enumerator.MoveNext())
				{
					hash = (13 * hash) + enumerator.Name.GetHashCode();
					hash = (13 * hash) + enumerator.Value.GetHashCode();
				}
				return hash;
			}
		}
	}
}