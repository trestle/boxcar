﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Trestle.Extensions;

namespace Boxcar
{
	public sealed class Repository
	{
		public Repository(string connectionString, string databaseName)
		{
			_client = new MongoClient(connectionString);
			_db = _client.GetDatabase(databaseName);
		}

		public void DeleteAll()
		{
			var bsonDocuments = _db.GetCollection<BsonDocument>("documents");
			bsonDocuments.DeleteMany(FilterDefinition<BsonDocument>.Empty);
		}

		public void Set(Document document)
		{
			var bsonDocuments = _db.GetCollection<BsonDocument>("documents");
			bsonDocuments.DeleteMany(new BsonDocument("name", document.Name));
			var bsonDocument = new BsonDocument(document.ToDictionary());
			bsonDocuments.InsertOne(bsonDocument);
		}

		public Document Get(string name)
		{
			var bsonDocuments = _db.GetCollection<BsonDocument>("documents");
			var cursor = bsonDocuments.FindSync<BsonDocument>(new BsonDocument("name", name));
			while (cursor.MoveNext())
			{
				IEnumerable<BsonDocument> batch = cursor.Current;
				foreach (BsonDocument bsonDocument in batch)
				{
					bsonDocument.Remove("_id");
					var json = ((object)(bsonDocument)).ToJson(typeof(BsonDocument));
					var memory = new MemoryStream(Encoding.UTF8.GetBytes(json));
					var document = new Trestle.Serialization.Json.JsonFormatter().Deserialize<Document>(memory);
					return document;
				}
			}
			return new Document();
		}

		public List<string> Search(string search)
		{
			var results = new List<string>();
			var bsonDocuments = _db.GetCollection<BsonDocument>("documents");

			var filter = FilterDefinition<BsonDocument>.Empty;
			var cursor = bsonDocuments.FindSync<BsonDocument>(filter);
			while (cursor.MoveNext())
			{
				IEnumerable<BsonDocument> batch = cursor.Current;
				foreach (BsonDocument bsonDocument in batch)
				{
					var name = bsonDocument.GetValue("name").ToString();
					if (name.Contains(search))
					{
						results.Add(name);
					}
					else
					{
						foreach (var value in bsonDocument.GetValue("tags").AsBsonArray.ToArray())
						{
							var tag = value.ToString();
							if (tag == search)
							{
								results.Add(name);
							}
						}
					}
				}
			}
			return results;
		}

		private MongoClient _client;
		private IMongoDatabase _db;
	}
}