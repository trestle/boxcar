﻿using LiteDB;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Trestle.Serialization.Json;

namespace Boxcar.LiteDB
{
	public class Collection<T> : ICollection<T> where T : ISerializable
	{
		public Collection(string connectionString, string name)
		{
			var db = new LiteDatabase(connectionString);
			_collection = db.GetCollection<BsonDocument>(name);
		}

		public void Add(T item)
		{
			Cache.Add(item);
			_collection.Insert(item.GetLiteDBBsonDocument());
		}

		public void Clear()
		{
			_collection.Delete(Query.All()); _cache = null;
		}

		public bool Contains(T item)
		{
			return Cache.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			var index = arrayIndex;
			Cache.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { return Cache.Count; }
		}

		public bool IsReadOnly { get { return false; } }

		public bool Remove(T item)
		{
			if (Cache.Remove(item))
			{
				foreach (var bsonDocument in _collection.FindAll())
				{
					var i = bsonDocument.Convert<T>();
					if (i.ToJson() == item.ToJson())
					{
						_collection.Delete(bsonDocument);
						//_cache = null;
						return true;
					}
				}
			}
			return false;
		}

		// https://stackoverflow.com/questions/11296810/how-do-i-implement-ienumerablet
		public IEnumerator<T> GetEnumerator()
		{
			return Cache.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return Cache.GetEnumerator();
		}

		// var results = customers.Find(x => x.Name.StartsWith("Jo"));
		private readonly LiteCollection<BsonDocument> _collection;

		private List<T> Cache
		{
			get
			{
				if (_cache is null)
				{
					_cache = new List<T>();
					foreach (var bsonDocument in _collection.FindAll())
					{
						var i = bsonDocument.Convert<T>();
						_cache.Add(i);
					}
				}
				return _cache;
			}
		}

		private List<T> _cache;
	}
}