﻿using LiteDB;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Trestle.Extensions;

namespace Boxcar.LiteDB
{
	public static class BsonDocumentExtension
	{
		public static BsonDocument GetLiteDBBsonDocument(this ISerializable item)
		{
			var data = item.GetSerializationInfo().GetPersistentData();
			//var data = info.GetPersistentData();
			//var data = item.Convert<IDictionary>();
			//var json = item.ToJson();
			//var data = new JsonFormatter().Deserialize<IDictionary>(new MemoryStream(Encoding.UTF8.GetBytes(json)));
			var document = new BsonDocument();
			foreach (string key in data.Keys)
			{
				document.Add(key, new BsonValue(data[key]));
			}
			return document;
		}

		public static BsonDocument GetLiteDBBsonDocument(this ISerializable item,KeyValuePair<string,BsonValue> kvp)
		{
			var data = item.GetSerializationInfo().GetPersistentData();
			var document = new BsonDocument();
			document.Add(kvp);
			foreach (string key in data.Keys)
			{
				document.Add(key, new BsonValue(data[key]));
			}
			return document;
		}

		public static T Convert<T>(this BsonDocument document) where T : ISerializable
		{
			if (document != null)
			{
				document.Remove("_id");
				var dictionary = document.GetDictionary();
				return dictionary.Convert<T>();

				//var json = document.ToString();
				//return new JsonFormatter().Deserialize<T>(new MemoryStream(Encoding.UTF8.GetBytes(json)));
			}
			return default(T);
		}

		private static Dictionary<string, object> GetDictionary(this BsonDocument document)
		{
			var data = new Dictionary<string, object>();
			foreach (var key in document.Keys)
			{
				var bsonValue = document[key];
				object value = bsonValue.RawValue;
				data.Add(key, value);
			}
			return data;
		}
	}
}