﻿using LiteDB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Boxcar.LiteDB
{
	public class Dictionary<T> : IDictionary<string, T> where T : ISerializable
	{
		public Dictionary(string connectionString, string name)
		{
			var db = new LiteDatabase(connectionString);
			_collection = db.GetCollection<BsonDocument>(name);
		}

		public void Add(string name, T item)
		{
			Cache.Add(name, item);
			var bsonDoc = item.GetLiteDBBsonDocument(new KeyValuePair<string, BsonValue>("_key", new BsonValue(name)));
			_collection.Insert(bsonDoc);
		}

		public void Add(KeyValuePair<string, T> kvp) { }
		public bool Contains(KeyValuePair<string, T> kvp) { return false; }
		public void CopyTo(KeyValuePair<string, T>[] kvps, int index) { }
		public bool Remove(KeyValuePair<string, T> kvp) { return false; }
		IEnumerator IEnumerable.GetEnumerator() { return Cache.GetEnumerator(); }
		public IEnumerator<KeyValuePair<string, T>> GetEnumerator()
		{
			return Cache.GetEnumerator();
		}

		public bool IsReadOnly { get { return false; } }

		public void Clear() { _collection.Delete(Query.All()); _cache = null; }
		public bool ContainsKey(string key) { return Cache.ContainsKey(key); }
		public int Count { get { return Cache.Count; } }
		public ICollection<string> Keys { get { return Cache.Keys; } }
		public ICollection<T> Values { get { return Cache.Values; } }
		public bool Remove(string key) { return Cache.Remove(key); }

		public T this[string name]
		{
			get { return default(T); }
			set { }
		}

		public bool TryGetValue(string key, out T value) { value = default(T); return false; }
		private Dictionary<string, T> Cache
		{
			get
			{
				if (_cache is null)
				{
					_cache = new Dictionary<string, T>();
					foreach (var bsonDocument in _collection.FindAll())
					{
						BsonValue bsonValue;
						var id = bsonDocument.TryGetValue("_id", out bsonValue);
						//var id = bsonDocument.GetElement("_id").Value.AsObjectId;
						var sid = bsonValue.AsString;
						var i = bsonDocument.Convert<T>();
						_cache.Add(sid,i);
					}
					/*
					var cursor = _collection.FindSync<BsonDocument>(FilterDefinition<BsonDocument>.Empty);
					while (cursor.MoveNext())
					{
						IEnumerable<BsonDocument> batch = cursor.Current;
						foreach (BsonDocument bsonDocument in batch)
						{
							var id = bsonDocument.GetElement("_id").Value.AsObjectId;
							var sid = id.ToString();
							var i = bsonDocument.Convert<T>();
							_cache.Add(sid, i);
						}
					}*/
				}
				return _cache;
			}
		}

		private Dictionary<string, T> _cache;
		private readonly LiteCollection<BsonDocument> _collection;
	}
}
