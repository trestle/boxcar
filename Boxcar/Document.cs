﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Boxcar
{
	[Serializable]
	public class Document : MetaData, ISerializable
	{
		public string Data { get; set; } = string.Empty;

		public Document()
		{
		}

		private Document(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			Data = info.GetString("data");
		}

		/// <summary>
		/// Get the SerializationInfo
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("data", Data);
		}

		
	}
}