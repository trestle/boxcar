﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Trestle.Extensions;

namespace Boxcar.MongoDB
{
	public static class BsonDocumentExtension
	{
		public static BsonDocument GetMongoDBBsonDocument(this ISerializable item)
		{
			var info = item.GetSerializationInfo();
			var data = info.GetPersistentData();
			var document = new BsonDocument(data);
			return document;
		}

		public static BsonDocument GetMongoDBBsonDocument(this ISerializable item,BsonElement element)
		{
			var info = item.GetSerializationInfo();
			var persistent_data = info.GetPersistentData();
			var data = new Dictionary<string, object>();
			data.Add(element.Name, BsonTypeMapper.MapToDotNetValue(element.Value));
			foreach(string key in persistent_data.Keys)
			{
				data.Add(key, persistent_data[key]);
			}
			var document = new BsonDocument(data);
			return document;
		}

		public static T Convert<T>(this BsonDocument document)
		{
			if (document != null)
			{
				document.Remove("_id");

				var dictionary = document.GetDictionary();
				return dictionary.Convert<T>();

				//var json = document.ToJson();// document.ToString();
				//return new JsonFormatter().Deserialize<T>(new MemoryStream(Encoding.UTF8.GetBytes(json)));
			}
			return default(T);
		}

		private static Dictionary<string, object> GetDictionary(this BsonDocument document)
		{
			var data = new Dictionary<string, object>();
			foreach (var key in document.Names)
			{
				var bsonValue = document[key];
				object value = BsonTypeMapper.MapToDotNetValue(bsonValue);// bsonValue.RawValue;
				data.Add(key, value);
			}
			return data;
		}
	}
}