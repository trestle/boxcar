﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Boxcar.MongoDB
{
	public class Collection<T> : ICollection<T> where T : ISerializable
	{
		public Collection(string connectionString, string databaseName, string name)
		{
			var client = new MongoClient(connectionString);
			var db = client.GetDatabase(databaseName);
			_collection = db.GetCollection<BsonDocument>(name);
		}

		public void Add(T item)
		{
			Cache.Add(item);
			_collection.InsertOne(item.GetMongoDBBsonDocument());
		}

		public void Clear()
		{
			_collection.DeleteMany(FilterDefinition<BsonDocument>.Empty); _cache = null;
		}// _collection.Delete(Query.All()); }

		public bool Contains(T item)
		{
			return Cache.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Cache.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get
			{
				return Cache.Count;
			}
		}

		public bool IsReadOnly { get { return false; } }

		public bool Remove(T item)
		{
			if (Cache.Remove(item))
			{
				var cursor = _collection.FindSync<BsonDocument>(FilterDefinition<BsonDocument>.Empty);
				while (cursor.MoveNext())
				{
					IEnumerable<BsonDocument> batch = cursor.Current;
					foreach (BsonDocument bsonDocument in batch)
					{
						var i = bsonDocument.Convert<T>();
						if (i.ToJson() == item.ToJson())
						{
							_collection.DeleteOne(bsonDocument);
							return true;
						}
					}
				}
			}
			return false;
		}

		public IEnumerator<T> GetEnumerator()
		{
			return Cache.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return Cache.GetEnumerator();
		}

		private List<T> Cache
		{
			get
			{
				if (_cache is null)
				{
					_cache = new List<T>();
					var cursor = _collection.FindSync<BsonDocument>(FilterDefinition<BsonDocument>.Empty);
					while (cursor.MoveNext())
					{
						IEnumerable<BsonDocument> batch = cursor.Current;
						foreach (BsonDocument bsonDocument in batch)
						{
							var i = bsonDocument.Convert<T>();
							_cache.Add(i);
						}
					}
				}
				return _cache;
			}
		}

		private List<T> _cache;

		private readonly IMongoCollection<BsonDocument> _collection;
	}
}