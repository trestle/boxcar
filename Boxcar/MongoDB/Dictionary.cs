﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Boxcar.MongoDB
{
	public class Dictionary<T> : IDictionary<string,T> where T: ISerializable
	{
		public Dictionary(string connectionString, string databaseName, string name)
		{
			var client = new MongoClient(connectionString);
			var db = client.GetDatabase(databaseName);
			_collection = db.GetCollection<BsonDocument>(name);
		}

		public void Add(string name,T item)
		{
			Cache.Add(name, item);
			var bsonDoc = item.GetMongoDBBsonDocument(new BsonElement("_key", new BsonString(name)));
			//bsonDoc.SetElement(new BsonElement("_key", new BsonString(name)));
			_collection.InsertOne(bsonDoc);
		}

		public void Add(KeyValuePair<string, T> kvp) { }
		public bool Contains(KeyValuePair<string,T> kvp) { return false; }
		public void CopyTo(KeyValuePair<string, T>[] kvps, int index) { }
		public bool Remove(KeyValuePair<string, T> kvp) { return false; }
		IEnumerator IEnumerable.GetEnumerator() { return Cache.GetEnumerator(); }
		public IEnumerator<KeyValuePair<string,T>> GetEnumerator()
		{
			return Cache.GetEnumerator();
		}

		public bool IsReadOnly { get { return false; } }

		public void Clear() { _collection.DeleteMany(FilterDefinition<BsonDocument>.Empty); _cache = null; }
		public bool ContainsKey(string key) { return Cache.ContainsKey(key); }
		public int Count { get { return Cache.Count; } }
		public ICollection<string> Keys { get { return Cache.Keys; } }
		public ICollection<T> Values { get { return Cache.Values; } }
		public bool Remove(string key) { return Cache.Remove(key); }

		public T this[string name]
		{
			get { return default(T); }
			set { }
		}

		public bool TryGetValue(string key,out T value) { value = default(T); return false; }
		private Dictionary<string, T> Cache
		{
			get
			{
				if (_cache is null)
				{
					_cache = new Dictionary<string, T>();
					var cursor = _collection.FindSync<BsonDocument>(FilterDefinition<BsonDocument>.Empty);
					while (cursor.MoveNext())
					{
						IEnumerable<BsonDocument> batch = cursor.Current;
						foreach (BsonDocument bsonDocument in batch)
						{
							var id = bsonDocument.GetElement("_id").Value.AsObjectId;
							var sid = id.ToString();
							var i = bsonDocument.Convert<T>();
							_cache.Add(sid,i);
						}
					}
				}
				return _cache;
			}
		}

		private Dictionary<string, T> _cache;
		private readonly IMongoCollection<BsonDocument> _collection;
	}
}