﻿using LiteDB;
using System.Collections.Generic;
using System.IO;
using Trestle.Data;

namespace Boxcar.Sources
{
	internal sealed class LiteDocument : Document
	{
		public LiteDocument()
		{
		}

		public int Id { get; set; }
	}

	public sealed class LiteDB : IDataSource
	{
		public LiteDB(string connectionString)
		{
			_connectionString = connectionString;
		}

		public bool IsReadOnly { get { return false; } }

		public void Set(string name, Stream data)
		{
			LiteDatabase.FileStorage.Upload(name, name, data);
		}

		public Stream Get(string name)
		{
			var memory = new MemoryStream();
			var db = LiteDatabase;
			if (db.FileStorage.Exists(name))
			{
				db.FileStorage.Download(name, memory);
				memory.Seek(0, SeekOrigin.Begin);
			}
			return memory;
		}

		public void Delete(string name)
		{
			var db = LiteDatabase;
			if (db.FileStorage.Exists(name))
			{
				db.FileStorage.Delete(name);
			}
		}

		public IList<string> GetNames(int limit, string pattern)
		{
			var results = new List<string>();
			var db = LiteDatabase;
			var infos = db.FileStorage.Find("");
			foreach (var info in infos)
			{
				if (info.Id.IndexOf("metadata.") != 0)
				{
					results.Add(info.Id);
					if (results.Count >= limit)
					{
						return results;
					}
				}
			}
			return results;
		}

		public LiteDatabase LiteDatabase
		{
			get
			{
				return _liteDatabase ?? (_liteDatabase = new LiteDatabase(_connectionString));
			}
		}

		private LiteDatabase _liteDatabase;
		private readonly string _connectionString;
	}
}