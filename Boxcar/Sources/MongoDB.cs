﻿using LiteDB;
using MongoDB.Driver;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Trestle.Data;
using Trestle.Extensions;

namespace Boxcar.Sources
{
	internal sealed class MongoDocument : Document
	{
		public MongoDocument()
		{
		}

		public ObjectId Id { get; set; }
	}

	public sealed class MongoDB : IDataSource
	{
		public MongoDB(string connectionString, string databaseName)
		{
			var client = new MongoClient(connectionString);
			_db = client.GetDatabase(databaseName);
		}

		public bool IsReadOnly { get { return false; } }

		public void Set(string name, Stream data)
		{
			var documents = _db.GetCollection<MongoDocument>("documents");

			var seekable = data.GetSeekable();
			var filter = new FilterDefinitionBuilder<MongoDocument>().Eq(doc => doc.Name, name);
			documents.DeleteMany(filter);
			var document = new MongoDocument
			{
				Name = name,
				Data = new StreamReader(data).ReadToEnd()
			};

			documents.InsertOne(document);
		}

		public Stream Get(string name)
		{
			var documents = _db.GetCollection<MongoDocument>("documents");

			var memory = new MemoryStream();
			var filter = new FilterDefinitionBuilder<MongoDocument>().Eq(doc => doc.Name, name);

			var cursor = documents.FindSync<MongoDocument>(filter);
			while (cursor.MoveNext())
			{
				IEnumerable<MongoDocument> batch = cursor.Current;
				foreach (MongoDocument document in batch)
				{
					memory = new MemoryStream(Encoding.UTF8.GetBytes(document.Data));
				}
			}
			return memory;
		}

		public void Delete(string name)
		{
			var documents = _db.GetCollection<MongoDocument>("documents");
			var filter = new FilterDefinitionBuilder<MongoDocument>().Eq(doc => doc.Name, name);
			documents.DeleteMany(filter);
		}

		public IList<string> GetNames(int limit, string pattern)
		{
			var results = new List<string>();

			var documents = _db.GetCollection<MongoDocument>("documents");
			var filter = FilterDefinition<MongoDocument>.Empty;
			var cursor = documents.FindSync<MongoDocument>(filter);
			while (cursor.MoveNext())
			{
				IEnumerable<MongoDocument> batch = cursor.Current;
				foreach (MongoDocument document in batch)
				{
					results.Add(document.Name);
					if (results.Count == limit) return results;
				}
			}

			return results;
		}

		/*
		public LiteDatabase LiteDatabase
		{
			get
			{
				return _liteDatabase ?? (_liteDatabase = new LiteDatabase(_connectionString));
			}
		}

		private LiteDatabase _liteDatabase;
		private readonly string _connectionString;
		*/
		private IMongoDatabase _db;
	}
}