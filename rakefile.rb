VERSION='0.0.8'
NAME='Boxcar'
SLN_FILES=[]
NUNIT=FileList.new()
require 'dev'

CLEAN.include('**/bin','**/obj','.sonarqube','.vs','*.coverage','**/TestResults')

USER=Environment.default.user
puts "USER = #{USER}"

task :setup  do
    Setup.setupStandardClassLib("#{NAME}",'C#') if(!Dir.exists?("#{NAME}"))
	Dir.chdir("#{NAME}.Test") do
		puts `dotnet add package coverlet.msbuild`
	end
    puts `nuget restore #{NAME}.sln`
end

task :build do
	puts `dotnet build --configuration Debug`
	puts `dotnet build --configuration Release`
end

task :package => [:build,:analyze] do
	
end

task :pull do
end

task :test do
    puts `dotnet test #{NAME}.Test/#{NAME}.Test.csproj --framework netcoreapp2_1`
	puts `dotnet test #{NAME}.Test/#{NAME}.Test.csproj --framework net46`
end

task :publish do
	FileUtils.cp("#{NAME}/bin/Release/#{NAME}.#{VERSION}.nupkg","#{NAME}.#{VERSION}.nupkg")
	local="#{Environment.dev_root}\\nuget"
	list=`nuget list #{NAME} -Source #{local}`
	if(!list.include?("#{NAME} #{VERSION}"))
		puts "nuget add #{NAME}.#{VERSION}.nupkg -Source #{local}"
		puts `nuget add #{NAME}.#{VERSION}.nupkg -Source #{local}`
	end

	#list=`nuget list #{NAME} -Source nuget.org`
	#if(!list.include?("#{NAME} #{VERSION}"))
	#	puts `nuget push #{NAME}.#{VERSION}.nupkg -Source https://api.nuget.org/v3/index.json`
	#end
end