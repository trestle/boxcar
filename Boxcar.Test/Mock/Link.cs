﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Boxcar.Mock
{
	[Serializable]
	internal sealed class Link : ISerializable
	{
		public Link(Uri url)
		{
			Url = url;
		}

		private Link(SerializationInfo info, StreamingContext context)
		{
			Url = new Uri(info.GetString("url"));
		}

		/// <summary>
		/// Get the SerializationInfo
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("type", "Link");
			info.AddValue("url", Url.ToString());
		}

		public Uri Url { get; }
	}
}