﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Boxcar.Mock
{
	[Serializable]
	public class Widget : ISerializable
	{
		public Widget()
		{
		}

		private Widget(SerializationInfo info, StreamingContext context)
		{
			Url = info.GetString("url");
			Name = info.GetString("name");
			Description = info.GetString("description");
		}

		/// <summary>
		/// Get the SerializationInfo
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("url", Url);
			info.AddValue("name", Name);
			info.AddValue("description", Description);
		}

		public string Name { get; set; } = string.Empty;
		public string Url { get; set; } = string.Empty;
		public string Description { get; set; } = string.Empty;

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			Widget objAsWidget = obj as Widget;
			if (objAsWidget == null) return false;
			else return Equals(objAsWidget);
		}

		public bool Equals(Widget other)
		{
			if (other is null) return false;
			if (!Name.Equals(other.Name)) return false;
			if (!Description.Equals(other.Description)) return false;
			if (!Url.Contains(other.Url)) return false;
			return true;
		}
	}
}