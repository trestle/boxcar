﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Trestle.Extensions;

namespace Boxcar.Mock
{
	[Serializable]
	public class State : ISerializable, IEquatable<State>
	{
		public string Name { get; set; }
		public string Abbreviation { get; set; }
		public string Capital { get; set; }

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			State objAsState = obj as State;
			if (objAsState == null) return false;
			else return Equals(objAsState);
		}

		public bool Equals(State other)
		{
			if (other is null) return false;
			if (!Name.Equals(other.Name)) return false;
			if (!Abbreviation.Equals(other.Name)) return false;
			if (!Capital.Contains(other.Capital)) return false;
			return true;
		}

		public override int GetHashCode()
		{
			return this.GetSerializationInfo().ComputeHashCode();
		}

		public State()
		{
		}

		private State(SerializationInfo info, StreamingContext context)
		{
			Name = info.GetString("name");
			Abbreviation = info.GetString("abbreviation");
			Capital = info.GetString("capital");
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("name", Name);
			info.AddValue("abbreviation", Abbreviation);
			info.AddValue("capital", Capital);
		}

		public static IEnumerable<State> States
		{
			get
			{
				if (_states is null)
				{
					var states = new List<State>();
					var data = new Trestle.Serialization.Json.JsonFormatter()
								.Deserialize<IDictionary>(
						typeof(State).Assembly
						 .GetManifestResourceStream(
							"Boxcar.Test.Mock.States.json"));

					foreach (string name in data.Keys)
					{
						var sdata = data[name] as IDictionary;
						states.Add(new State
						{
							Name = name,
							Abbreviation = sdata["Abbreviation"].ToString(),
							Capital = sdata["Capital"].ToString()
						});
					}
					_states = states;
				}
				return _states;
			}
		}

		private static IEnumerable<State> _states;
	}
}