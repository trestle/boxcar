﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Trestle.Extensions;

namespace Boxcar.Mock
{
	[Serializable]
	public class County : ISerializable
	{
		public string Name { get; set; }
		public string State { get; set; }
		public string Url { get; set; }

		public County() { }

		private County(SerializationInfo info, StreamingContext context)
		{
			Name = info.GetString("name");
			State = info.GetString("state");
			Url = info.GetString("url");
		}

		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("name", Name);
			info.AddValue("state", State);
			info.AddValue("url", Url);
		}

		public static IEnumerable<County> Counties
		{
			get
			{
				if (_counties is null)
				{
					List<County> counties = new List<County>();
					var data = new Trestle.Serialization.Json.JsonFormatter()
								.Deserialize<IDictionary>(
						typeof(County).Assembly
						 .GetManifestResourceStream(
							"Boxcar.Test.Mock.States.json"));

					foreach (string name in data.Keys)
					{
						var sdata = data[name] as IDictionary;
						var state = sdata["Abbreviation"].ToString();
						var scounties = sdata["Counties"] as IDictionary;
						foreach (string county_name in scounties.Keys)
						{
							var scounty = scounties[county_name] as IDictionary;
							counties.Add(new County
							{
								Name = county_name,
								State = state,
								Url = scounty["WikiUrl"].ToString()
							});
						}
					}
					_counties = counties;
				}
				return _counties;
			}
		}

		private static IEnumerable<County> _counties;
	}
}