﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NUnit.Framework;
using Trestle.Extensions;

namespace Boxcar.Test
{
	[TestFixture]
	internal static class IDictionaryTest
	{
		[Test]
		public static void Test_MongoDB_Dictionary()
		{
			var states = new MongoDB.Dictionary<Mock.State>("mongodb://localhost:27017", "BoxcarTest", "states");
			
			states.Clear();
			states.Add("CO", new Mock.State { Name = "Colorado", Abbreviation = "CO", Capital = "Denver" });
			Assert.AreEqual(1, states.Count, "states.Count");
			states.Clear();

			var statesDictionary = new Dictionary<string, Mock.State>();
			foreach(var state in Mock.State.States)
			{
				statesDictionary.Add(state.Abbreviation, state);
			}
			Test<Mock.State>(states, statesDictionary);

			var countiesDictionary = new Dictionary<string, Mock.County>();
			foreach(var county in Mock.County.Counties)
			{
				countiesDictionary.Add($"{county.State}_{county.Name}", county);
			}
			var counties = new MongoDB.Dictionary<Mock.County>("mongodb://localhost:27017", "BoxcarTest", "counties");
			counties.Clear();
			Test<Mock.County>(counties, countiesDictionary);

			Mock.State colorado = states.Single(kvp => kvp.Value.Name == "Colorado").Value;
			Assert.NotNull(colorado, nameof(colorado));
			Assert.AreEqual("Colorado", colorado.Name);

			Mock.State california = states.First(kvp => kvp.Value.Name == "California").Value;
			Assert.NotNull(california, nameof(california));
			Assert.AreEqual("California", california.Name);
		}

		[Test]
		public static void Test_LiteDB_Dictionary()
		{
			var filename = MethodBase.GetCurrentMethod().GetTempFilename("test.db");
			if (File.Exists(filename)) File.Delete(filename);

			var states = new LiteDB.Dictionary<Mock.State>(filename, "states");

			states.Clear();
			states.Add("CO", new Mock.State { Name = "Colorado", Abbreviation = "CO", Capital = "Denver" });
			Assert.AreEqual(1, states.Count, "states.Count");
			states.Clear();

			var statesDictionary = new Dictionary<string, Mock.State>();
			foreach (var state in Mock.State.States)
			{
				statesDictionary.Add(state.Abbreviation, state);
			}
			Test<Mock.State>(states, statesDictionary);

			var countiesDictionary = new Dictionary<string, Mock.County>();
			foreach (var county in Mock.County.Counties)
			{
				countiesDictionary.Add($"{county.State}_{county.Name}", county);
			}
			var counties = new MongoDB.Dictionary<Mock.County>("mongodb://localhost:27017", "BoxcarTest", "counties");
			counties.Clear();
			Test<Mock.County>(counties, countiesDictionary);

			Mock.State colorado = states.Single(kvp => kvp.Value.Name == "Colorado").Value;
			Assert.NotNull(colorado, nameof(colorado));
			Assert.AreEqual("Colorado", colorado.Name);

			Mock.State california = states.First(kvp => kvp.Value.Name == "California").Value;
			Assert.NotNull(california, nameof(california));
			Assert.AreEqual("California", california.Name);
		}
		public static void Test<T>(IDictionary<string,T> dictionary, IDictionary<string,T> test_instances)
		{
			if (!dictionary.IsReadOnly)
			{
				foreach (var key in test_instances.Keys)
				{
					var item = test_instances[key];
					dictionary.Add(key,item);
				}
				Assert.AreEqual(test_instances.Count, dictionary.Count);
			}
		}
	}
}
