using NUnit.Framework;

namespace Boxcar
{
	public class RepositoryTest
	{
		[Test]
		public void MongoDB_Usage()
		{
			var repository = new Repository("mongodb://localhost:27017", "BoxcarTest");
			var doc = DocumentTest.GetFoxDocument();
			repository.DeleteAll();
			repository.Set(doc);
			var doc2 = repository.Get(doc.Name);
			Assert.NotNull(doc2, nameof(doc2));
			Assert.AreEqual(doc.Name, doc2.Name);
			Assert.AreEqual(doc.Data, doc2.Data);

			var names = repository.Search("fox");
			Assert.True(names.Contains("fox.txt"));

			names = repository.Search("dog");
			Assert.True(names.Contains("fox.txt"));
		}

		/*
        [Test]
        public void Usage()
        {
			var filename = MethodBase.GetCurrentMethod().GetTempFilename("test.db");
			if (File.Exists(filename)) File.Delete(filename);

			var repo = new Repository(filename);
			Assert.AreEqual(0, repo.GetNames().Count,"repo.GetNames().Count");

			repo.Set("fox", "The quick brown fox jumps over the lazy dog.");
			Assert.AreEqual(1, repo.GetNames().Count, "repo.GetNames().Count");
			Assert.AreEqual("The quick brown fox jumps over the lazy dog.",
							repo.GetString("fox"));
			repo.Set("lorem", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
			Assert.AreEqual(2, repo.GetNames().Count, "repo.GetNames().Count");

			var names = repo.Search("brown");
			Assert.AreEqual(1, names.Count, "names.Count");
		}*/

		/*
		[Test]
		public void Usage_Link()
		{
			var filename = MethodBase.GetCurrentMethod().GetTempFilename("links.db");
			if (File.Exists(filename)) File.Delete(filename);

			var repo = new Repository(filename);
			Assert.AreEqual(0, repo.GetNames().Count, "repo.GetNames().Count");

			var urls = new List<string>
			{
				"http://www.google.com/",
				"http://www.gitlab.com/",
				"https://www.nuget.com"
			};

			foreach(var url in urls)
			{
				repo.Set(Guid.NewGuid().ToString(), new Mock.Link(new Uri(url)));
			}

			var repos = repo.Search("http://www.google.com/");
			Assert.AreEqual(1, repos.Count);

			var link = repo.Get<Mock.Link>(repos[0]);
			Assert.NotNull(link, nameof(link));
			Assert.AreEqual("http://www.google.com/", link.Url.ToString());
		}*/
	}
}