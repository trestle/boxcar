﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Trestle.Extensions;

namespace Boxcar.Test
{
	[TestFixture]
	internal static class ICollectionTest
	{
		[Test]
		public static void Test_MongoDB_Collection()
		{
			var states = new MongoDB.Collection<Mock.State>("mongodb://localhost:27017", "BoxcarTest", "states");
			states.Clear();
			Test<Mock.State>(states, Mock.State.States);

			var counties = new MongoDB.Collection<Mock.County>("mongodb://localhost:27017", "BoxcarTest", "counties");
			counties.Clear();
			Test<Mock.County>(counties, Mock.County.Counties);

			Mock.State colorado = states.Single(state => state.Name == "Colorado");
			Assert.NotNull(colorado, nameof(colorado));

			Mock.State california = Mock.State.States.First(state => state.Name == "California");
			Assert.NotNull(california, nameof(california));
			// var query =
			//fruits.Select((fruit, index) =>
			//				  new { index, str = fruit.Substring(0, index) });

			//var query = states.Select(state => state.Name == "Colorado");
			//var co = query.First() as Mock.State;
			//Assert.AreEqual("CO", co.Abbreviation);
		}

		[Test]
		public static void Test_LiteDB_Collection()
		{
			var filename = MethodBase.GetCurrentMethod().GetTempFilename("test.db");
			if (File.Exists(filename)) File.Delete(filename);

			var states = new LiteDB.Collection<Mock.State>(filename, "states");
			Test<Mock.State>(states, Mock.State.States);

			var counties = new LiteDB.Collection<Mock.County>(filename, "counties");
			Test<Mock.County>(counties, Mock.County.Counties);

			Mock.State colorado = states.Single(state => state.Name == "Colorado");
			Assert.NotNull(colorado, nameof(colorado));

			Mock.State california = states.First(state => state.Name == "California");
			Assert.NotNull(california, nameof(california));
			//var results = customers.Find(x => x.Name.StartsWith("Jo"));
		}

		public static void Test<T>(ICollection<T> collection, IEnumerable<T> test_instances)
		{
			if (!collection.IsReadOnly)
			{
				foreach (var item in test_instances)
				{
					collection.Add(item);
				}
				Assert.AreEqual(test_instances.Count(), collection.Count);
			}
		}
	}
}