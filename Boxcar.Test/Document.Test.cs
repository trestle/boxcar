﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Trestle.Extensions;

namespace Boxcar
{
	[TestFixture]
	internal static class DocumentTest
	{
		[Test]
		public static void Usage()
		{
			var doc = GetFoxDocument();

			var doc2 = doc.Clone() as Document;
			Assert.NotNull(doc2, nameof(doc2));

			Assert.AreEqual("fox.txt", doc2.Name);
			Assert.AreEqual("the quick brown fox jumps over the lazy dog", doc2.Data);
			Assert.AreEqual(doc.Created, doc2.Created);
			Assert.True(doc2.Tags.Contains("fox"), "doc2.Tags.Contains(\"fox\")");
			Assert.AreEqual("test of storing text data in a document", doc2.Properties["description"], "doc2.Properties['description']");
		}

		public static Document GetFoxDocument()
		{
			return new Document
			{
				Name = "fox.txt",
				Data = "the quick brown fox jumps over the lazy dog",
				Tags = new List<string> { "fox", "dog" },
				Properties = new Dictionary<string, object>
				{
					{ "description", "test of storing text data in a document" }
				},
				Created = DateTime.Now
			};
		}
	}
}