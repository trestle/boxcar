﻿using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Trestle.Extensions;

namespace Boxcar.MongoDB
{
	[TestFixture]
	internal static class CollectionTest
	{
		[Test]
		public static void Usage()
		{
			var filename = MethodBase.GetCurrentMethod().GetTempFilename("test.db");
			if (File.Exists(filename)) File.Delete(filename);

			var collection = new Collection<Mock.Widget>("mongodb://localhost:27017", "BoxcarTest", "widgets");
			collection.Clear();
			Assert.AreEqual(0, collection.Count, "list.Count");

			Assert.False(collection.Contains(new Mock.Widget()));

			var mocks = GetMockWidgets();
			foreach (var widgetA in mocks)
			{
				collection.Add(widgetA);

				Assert.True(collection.Contains(widgetA), "contains");
			}

			Assert.AreEqual(mocks.Count(), collection.Count, "collection.Count");

			Mock.Widget[] array = new Mock.Widget[25];
			collection.CopyTo(array, 0);

			Assert.False(collection.Remove(new Mock.Widget()), "collection.Remove(not there)");
			Assert.True(collection.Remove(array[0]), "collection.Remove(first item)");

			collection.Clear();
			Assert.AreEqual(0, collection.Count, "collection.Count");
		}

		public static IEnumerable<Mock.Widget> GetMockWidgets()
		{
			yield return new Mock.Widget { Name = "google", Url = "http://www.google.com/", Description = "a search engine" };
		}
	}
}